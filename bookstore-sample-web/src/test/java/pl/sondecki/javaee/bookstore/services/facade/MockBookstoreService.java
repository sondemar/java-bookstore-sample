package pl.sondecki.javaee.bookstore.services.facade;

import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import java.util.List;

/**
 * Alternative/Mock class for {@link pl.sondecki.javaee.bookstore.services.facade.impl.BookstoreServiceEJB}
 * during running test cases.
 * <p>
 *
 * @author Mariusz Sondecki
 */
@Alternative
public class MockBookstoreService implements BookstoreService {

    @Inject
    private Event<BookVO> bookEvent;

    @Override
    public void addBook(BookVO book) {
        bookEvent.fire(book);
    }

    @Override
    public BookVO getBook(long bookId) {
        BookVO bookVO = null;
        if (bookId % 2 == 0) {
            bookVO = new BookVO();
        }
        return bookVO;
    }

    @Override
    public List<BookVO> getAllBooks() {
        return null;
    }

    @Override
    public List<BookVO> findByIsbn(String isbn) {
        return null;
    }
}
