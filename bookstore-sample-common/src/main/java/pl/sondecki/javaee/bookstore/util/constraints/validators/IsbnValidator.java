package pl.sondecki.javaee.bookstore.util.constraints.validators;

import org.jboss.logging.Logger;
import pl.sondecki.javaee.bookstore.util.constraints.Isbn;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * Custom constraint validator for {@link Isbn} stereotype
 *
 * @author Mariusz Sondecki
 */
public class IsbnValidator
        implements ConstraintValidator<Isbn, String> {

    @Inject
    private Logger logger;

    @Override
    public void initialize(Isbn isbn) {
        logger.debug("Initialization of validator");
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        // based on: http://regexlib.com/REDetails.aspx?regexp_id=4917
        String regex = "^(ISBN[-]*(1[03])*[ ]*(: ){0,1})*(([0-9Xx][- ]*){13}|([0-9Xx][- ]*){10})$";

        return Pattern.compile(regex).matcher(value).matches();
    }
}
