/**
 * Business logic layer which manages business processing rules and logic.
 *
 * @author Mariusz Sondecki
 */
package pl.sondecki.javaee.bookstore.services;