package pl.sondecki.javaee.bookstore.model.common.book;

/**
 * Common model interface for Book entity (for example it can be XML or Database entity)
 *
 * @author Mariusz Sondecki
 */
public interface BookInterface {

    Long getId();

    String getTitle();

    void setTitle(String title);

    String getIsbn();

    void setIsbn(String isbn);

    String getForename();

    void setForename(String forename);

    String getSurname();

    void setSurname(String surname);
}
