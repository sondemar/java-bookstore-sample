package pl.sondecki.javaee.bookstore.dao.database.book;

import pl.sondecki.javaee.bookstore.dao.common.book.BookDAO;
import pl.sondecki.javaee.bookstore.model.entity.db.book.Book;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Specializes;
import java.util.Arrays;
import java.util.List;

/**
 * Alternative/Mock class for {@link BookDatabaseDAO} during running test cases.
 * <p>
 * @author Mariusz Sondecki
 */
@Alternative
public class MockBookDatabaseDAO implements BookDAO<Book> {

    public static final String ISBN_BEAN1 = "ISBN-10 0-596-52068-9";
    public static final String ISBN_BEAN2 = "0-596-52068-9";

    private List<Book> list = null;

    @PostConstruct
    private void init() {
        list = Arrays.asList(new Book("title", ISBN_BEAN1, "Adam", "Abramowski"),
                new Book("title", ISBN_BEAN2, "Adam", "Abba"));
    }

    @Override
    public List<Book> findByIsbn(String isbn) {
        return Arrays.asList(list.get(0));
    }

    @Override
    public Book get(Long id) {
        return list.get(0);
    }

    @Override
    public List<Book> getAll() {
        return list;
    }

    @Override
    public void merge(Book Book) {
    }

    @Override
    public void remove(Book Book) {
    }

    @Override
    public void persist(Book Book) {
    }
}
