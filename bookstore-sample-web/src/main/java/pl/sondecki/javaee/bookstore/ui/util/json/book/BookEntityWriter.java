package pl.sondecki.javaee.bookstore.ui.util.json.book;

import java.awt.print.Book;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;

/**
 * Service util which is responsible for conversion of a {@link Book} type to a stream (in this case JSON type).
 * This util is used by REST clients.
 *
 * @author Mariusz Sondecki
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class BookEntityWriter implements MessageBodyWriter<BookVO> {

    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return BookVO.class.isAssignableFrom(aClass);
    }

    @Override
    public long getSize(BookVO book, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return -1;//deprecated method
    }

    @Override
    public void writeTo(BookVO book, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        JsonGenerator generator = Json.createGenerator(outputStream);
        generator.writeStartObject().write("forename", book.getForename()).write("surname", book.getSurname()).
                write("title", book.getTitle()).write("isbn", book.getIsbn()).writeEnd();
        generator.flush();

    }
}
