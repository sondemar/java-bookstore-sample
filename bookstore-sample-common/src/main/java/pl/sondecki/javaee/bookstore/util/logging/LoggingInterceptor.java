package pl.sondecki.javaee.bookstore.util.logging;

import org.jboss.logging.Logger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Mariusz Sondecki on 2015-08-01.
 */
@Loggable
@Interceptor
public class LoggingInterceptor {

    private static final String ENTERING_PATTERN = "Entering: %s#%s(%s)";
    private static final String EXITING_PATTERN = "Exiting: %s#%s(%s)";
    @Inject
    private Logger logger;

    @AroundInvoke
    private Object aroundInvoke(InvocationContext ic) throws Exception {
        String paramsString = "";

        final Object[] parameters = ic.getParameters();
        if (parameters != null) {

            StringBuilder stringBuilder = new StringBuilder();
            List<Object> objects = Arrays.asList(parameters);
            Iterator<Object> iterator = objects.iterator();

            while (iterator.hasNext()) {
                Object o = iterator.next();
                stringBuilder.append(o != null ? String.valueOf(o) : null);
                if (iterator.hasNext()) {
                    stringBuilder.append(",");
                }
            }
            paramsString = stringBuilder.toString();
        }

        logger.infof(ENTERING_PATTERN, ic.getTarget().getClass().getName(), ic.getMethod().getName(), paramsString);
        try {
            return ic.proceed();
        } catch (Exception e) {
            logger.error(null, e);
            throw e;
        } finally {
            logger.infof(EXITING_PATTERN, ic.getTarget().getClass().getName(), ic.getMethod().getName(), paramsString);
        }
    }
}
