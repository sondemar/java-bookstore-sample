package pl.sondecki.javaee.bookstore.dao.common;

import java.util.List;

/**
 * A base common DAO providing basic CRUD operations for a specific Entity
 *
 * @param <Entity> class of the entity supported by {@link CommonDAO}
 * @author Mariusz Sondecki
 */
public interface CommonDAO<Entity> {

    /**
     * Find an entity instance by primary key.
     *
     * @param id primary key
     * @return the found <Entity> instance or null if the entity does not exist
     */
    Entity get(Long id);

    /**
     * Persit the entity instance
     *
     * @param entity
     */
    void persist(Entity entity);

    /**
     * Remove the entity instance.
     *
     * @param entity
     */
    void remove(Entity entity);

    /**
     * Merge the state of the given entity into the current persistence context.
     *
     * @param entity
     */
    void merge(Entity entity);

    /**
     * Find all entity instances.
     *
     * @return
     */
    List<Entity> getAll();
}
