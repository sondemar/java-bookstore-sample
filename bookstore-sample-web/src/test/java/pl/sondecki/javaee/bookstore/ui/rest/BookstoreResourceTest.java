package pl.sondecki.javaee.bookstore.ui.rest;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URL;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Test case for the REST service {@link BookstoreResource}.
 * <p>
 *
 * @author Mariusz Sondecki
 */
@RunWith(Arquillian.class)
public class BookstoreResourceTest {

    private static WebTarget target;
    @ArquillianResource
    private URL base;

    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addPackages(true, BookstoreResource.class.getPackage())
                .addPackages(true, "pl.sondecki.javaee.bookstore.ui.util")
                .addPackages(true, "pl.sondecki.javaee.bookstore.model")
                .addPackage("pl.sondecki.javaee.bookstore.services.facade")
                .addPackages(true, "pl.sondecki.javaee.bookstore.util")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource(EmptyAsset.INSTANCE, "messages.properties")
                .addAsWebInfResource("test-beans.xml", "beans.xml");
    }

    @Before
    public void setUp() throws Exception {
        Client client = ClientBuilder.newClient();
        target = client.target(URI.create(new URL(base, "rest/books").
                toExternalForm()));
    }

    @Test
    public void should_add_validated_book() throws Exception {
        JsonObject book = Json.createObjectBuilder()
                .add("forename", "Adam")
                .add("surname", "Aksamitny")
                .add("title", "title")
                .add("isbn", "ISBN-10 0-596-52068-9")
                .build();

        Response response = postBook(book);

        assertStatus(response, Response.Status.Family.SUCCESSFUL);
    }


    @Test
    public void should_not_add_invalidated_book() throws Exception {
        JsonObject book = Json.createObjectBuilder()
                .add("forename", "Adam")
                .add("surname", "ksamitny")
                .add("title", "title")
                .add("isbn", "ISBN-10 0-596-52068--9")
                .build();


        Response response = null;
        try {
            response = postBook(book);
        } catch (ServerErrorException e) {
            assertStatus(response, Response.Status.Family.SERVER_ERROR);
        }

    }

    @Test
    public void should_give_book() throws Exception {
        final BookVO bookVO = target.path("2")
                .request(MediaType.APPLICATION_XML)
                .get(BookVO.class);

        assertThat(bookVO, notNullValue());
    }


    @Test
    public void should_not_give_book() throws Exception {

        try {
            target.path("2")
                    .request(MediaType.APPLICATION_XML)
                    .get(BookVO.class);
        } catch (ClientErrorException e) {
            assertStatus(e.getResponse(), Response.Status.Family.SERVER_ERROR);
        }
    }


    @Test
    public void should_success_for_books_request() throws Exception {

        try {
            target.path("2")
                    .request(MediaType.APPLICATION_XML)
                    .get(BookVO.class);
        } catch (ClientErrorException e) {
            assertStatus(e.getResponse(), Response.Status.Family.SERVER_ERROR);
        }
    }


    private Response postBook(JsonObject book) {
        Entity<JsonObject> bookEntity = Entity.json(book);
        return target
                .request()
                .post(bookEntity);
    }

    private void assertStatus(Response response, Response.Status.Family expectedStatus) {
        final Response.Status.Family actualStatus = response.getStatusInfo().getFamily();
        assertThat(actualStatus, equalTo(expectedStatus));
    }
}
