package pl.sondecki.javaee.bookstore.ui.controller;

import org.jboss.logging.Logger;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;
import pl.sondecki.javaee.bookstore.services.facade.BookstoreService;
import pl.sondecki.javaee.bookstore.ui.rest.BookStoreApplication;
import pl.sondecki.javaee.bookstore.ui.rest.BookstoreResource;
import pl.sondecki.javaee.bookstore.ui.util.FacesContextDecorator;
import pl.sondecki.javaee.bookstore.ui.util.json.book.BookEntityWriter;
import pl.sondecki.javaee.bookstore.ui.util.messages.MessagesManager;
import pl.sondecki.javaee.bookstore.ui.util.validation.book.BookValidator;
import pl.sondecki.javaee.bookstore.util.logging.Loggable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * JSF Managed bean which is model and controller.
 *
 * @author Mariusz Sondecki
 */
@Model
@Loggable
public class BookstoreController {

    @Inject
    private FacesContextDecorator facesContext;

    @Inject
    private BookstoreService bookstoreService;

    @Inject
    private HttpServletRequest httpServletRequest;

    @Inject
    private BookValidator bookValidator;

    @Inject
    private MessagesManager messagesManager;

    @Inject
    private Logger logger;

    private Client client;

    private WebTarget target;

    private BookVO newBook;

    private boolean byRest;

    @PostConstruct
    private void init() {
        initNewBook();
        initRESTClient();
    }

    @PreDestroy
    private void destroy() {
        destroyRESTClient();
    }

    @Produces
    @Named
    public BookVO getNewBook() {
        return newBook;
    }

    public boolean isByRest() {
        return byRest;
    }

    public void setByRest(boolean byRest) {

        this.byRest = byRest;
    }

    /**
     * This is JSF action method. Add filled {@link BookVO} instance to the system by using facade service or by using REST resource if {@link this#isByRest()} is set.
     * After storing a new record, action clean data inside {@link BookVO} instance and redirect view to 'list'.
     *
     * @return null if there is some error or 'list' (name of new view) if everything is correct.
     * @throws Exception
     */
    public String addBook() throws Exception {
        try {
            if (byRest) {
                try {
                    target.register(BookEntityWriter.class).request().post(Entity.entity(newBook, MediaType.APPLICATION_JSON));
                } catch (ClientErrorException | ServerErrorException e) {
                    addClientMessage(FacesMessage.SEVERITY_ERROR, e.getResponse().getEntity().toString(), messagesManager.getMessage("addbook.error.detail"));
                    return null;
                }
                logger.debug("Added a book bean via REST");
            } else {
                if (bookValidator.isbnAlreadyExists(newBook.getIsbn())) {
                    addClientMessage(FacesMessage.SEVERITY_ERROR, messagesManager.getMessage("addbook.error.uniqueisbn.summary"), messagesManager.getMessage("addbook.error.detail"));
                    return null;
                }
                bookstoreService.addBook(newBook);
                logger.debug("Added a book bean in normal way");
            }

            addClientMessage(FacesMessage.SEVERITY_INFO, messagesManager.getMessage("addbook.success.info.summary"), messagesManager.getMessage("addbook.success.info.detail"));
            initNewBook();

            return "list";
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
            String errorMessage = getRootErrorMessage(e);
            addClientMessage(FacesMessage.SEVERITY_ERROR, errorMessage, messagesManager.getMessage("addbook.error.detail"));
            return null;
        }
    }

    /**
     * Addd message via JSF facesContext to the main messages not associated with UIs.
     *
     * @param severity
     * @param summary
     * @param detail
     */
    private void addClientMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesMessage m = new FacesMessage(severity, summary, detail);
        facesContext.addMessage(null, m);
    }

    /**
     * Get main cause of the exception
     *
     * @param e
     * @return
     */
    private String getRootErrorMessage(Exception e) {

        String errorMessage = messagesManager.getMessage("addbook.error.mock.summary");
        if (e == null) {
            return errorMessage;
        }

        Throwable t = e;
        while (t != null) {

            errorMessage = t.getLocalizedMessage();
            t = t.getCause();
        }

        return errorMessage;
    }

    private void initRESTClient() {
        client = ClientBuilder.newClient();
        String url = "http://" +
                httpServletRequest.getLocalName() +
                ":" +
                httpServletRequest.getLocalPort() +
                "/" +
                httpServletRequest.getContextPath() +
                BookStoreApplication.PATH + BookstoreResource.PATH;
        target = client
                .target(url);
    }

    private void destroyRESTClient() {
        if (client != null) {
            client.close();
        }
        logger.debug("Client destroyed.");
    }

    private void initNewBook() {
        newBook = new BookVO();
    }

}
