package pl.sondecki.javaee.bookstore.ui.rest.filter;

import org.jboss.logging.Logger;
import pl.sondecki.javaee.bookstore.ui.rest.filter.util.ServerLogBuilder;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Calendar;

/**
 * REST Container Request/Response filter/interceptor
 * responsible for logging informations about requested REST resources and duration between request and response.
 * Everything is logged into server logs with pattern: {@link ServerLogBuilder#Log_PATTERN}.
 *
 * @author Mariusz Sondecki
 */
@Provider
@Priority(Priorities.USER)
public class ServerLoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

    public static final String START_DATE = "start-date";

    @Inject
    private Logger logger;

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        requestContext.setProperty(START_DATE, Calendar.getInstance());
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        Calendar startDate = (Calendar) requestContext.getProperty(START_DATE);
        if (startDate != null) {
            long duration = Calendar.getInstance().getTimeInMillis()
                    - startDate.getTimeInMillis();

            logger.info(new ServerLogBuilder().appendStartDate(startDate.getTime()).appendStatus(responseContext.getStatusInfo().getFamily().name()).
                    appendRestMethod(requestContext.getMethod()).appendUrl(requestContext.getUriInfo().getAbsolutePath().toString()).
                    appendResourceClass(resourceInfo.getResourceClass().getSimpleName())
                    .appendResourceMethod(resourceInfo.getResourceMethod().getName()).appendDuration(duration).build());
        }
    }
}
