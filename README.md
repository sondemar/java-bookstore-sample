bookstore-sample: Application based on Java EE 7 technologies deployed as an EAR on WildFly server
==============================================================================================================
Author: Mariusz Sondecki

Application description
-------------------

1. Application has a page contains list of book records with fields: author, title, ISBN. When no records are stored the information ‘No records’ will be displayed.
2. Application contains a link to add new record.
3. A new record page displays simple form with fields: author, title, ISBN and button 'Add'. 
4. Application should have a validation rule on author that forename or surname should start from letter 'A'. Application must display failure message, mark failed field and hold user on new records form when validation not pass.
5. On success application redirects user to a list of added records.
6. Application writes an information to the server logs.
7. Additionally application provides RESTful services for add a book and return all existing books in the storage.

System requirements
-------------------

Java 8.0, Maven 3.2.x and WildFly 8.2.0 at least

Build and Deploy/Undeploy
-------------------------

1. To build and deploy the application type:

        mvn clean package wildfly:deploy
        
2. To undeploy the application type:

        mvn wildfly:undeploy
         
3. Application will be run with URL: <http://localhost:8080/bookstore-sample>

Run the Arquillian Tests 
-------------------------

1. To run the Arquillian Tests with remote profile type: 

        mvn clean test -Pwildfly-remote-arquillian

2. To run the Arquillian Tests with managed profile type: 

        mvn clean test -Pwildfly-managed-arquillian


Configure the custom Loggers and Log Handlers
---------------------------------------------------

1. To configure the custom Loggers and Log Handlers type:

        For Windows: JBOSS_HOME\bin\jboss-cli.bat --connect --file=configure-logging.cli

2. To remove the custom Loggers and Log Handlers type:

        For Windows: JBOSS_HOME\bin\jboss-cli.bat --connect --file=remove-logging.cli 

		