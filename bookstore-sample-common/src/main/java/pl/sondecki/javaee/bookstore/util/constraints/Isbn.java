package pl.sondecki.javaee.bookstore.util.constraints;

import org.hibernate.validator.constraints.NotEmpty;
import pl.sondecki.javaee.bookstore.util.constraints.validators.IsbnValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.*;

/**
 * Custom constraint stereotype which validates ISBN with by using {@link IsbnValidator}
 *
 * @author Mariusz Sondecki
 */
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Constraint(validatedBy = IsbnValidator.class)
@NotEmpty(message = "{org.hibernate.validator.constraints.NotEmpty.message}")
public @interface Isbn {

    String message() default "{pl.sondecki.javaee.bookstore.util.constraints.Isbn.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
