package pl.sondecki.javaee.bookstore.dao.common.book;


import pl.sondecki.javaee.bookstore.dao.common.CommonDAO;
import pl.sondecki.javaee.bookstore.model.common.book.BookInterface;

import java.util.List;


/**
 * A base common DAO providing basic CRUD operations for a specific Entity extending {@link BookInterface}
 *
 * @param <Entity> class of the entity supported by {@link BookDAO}
 * @author Mariusz Sondecki
 */
public interface BookDAO<Entity extends BookInterface> extends CommonDAO<Entity> {

    /**
     * Find an entity instance by unique, not null ISBN.
     *
     * @param isbn not null, unique ISBN
     * @return the found <Entity> instance or null if the entity does not exist
     */
    List<Entity> findByIsbn(String isbn);
}
