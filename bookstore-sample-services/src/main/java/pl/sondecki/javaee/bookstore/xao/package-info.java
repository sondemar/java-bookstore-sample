/**
 * XML access object layer which manages CRUD.
 *
 * @author Mariusz Sondecki
 */
package pl.sondecki.javaee.bookstore.xao;