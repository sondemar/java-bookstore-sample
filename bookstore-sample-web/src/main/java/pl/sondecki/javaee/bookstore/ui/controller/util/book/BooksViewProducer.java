package pl.sondecki.javaee.bookstore.ui.controller.util.book;


import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;
import pl.sondecki.javaee.bookstore.services.facade.BookstoreService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Class acting role of Observer event consumer and injection point producer. It listens to fired events
 * from sender and method is only called if the fired instance of the {@link BookVO}
 * declaring the observer method already exists. Class produces the list of stored {@link BookVO}s which is accessed
 * via {@link Named}.
 *
 * @author Mariusz Sondecki
 */
@RequestScoped
public class BooksViewProducer {

    @Inject
    private BookstoreService bookstoreService;

    private List<BookVO> books;

    @PostConstruct
    private void init() {
        loadViewBooks();
    }

    /**
     * Get a list of stored {@link BookVO}s. Method produces injection point for this list.
     * {@link Named} allows to use returned list via EL variable in the UI.
     *
     * @return list of stored {@link BookVO}s.
     */
    @Produces
    @Named
    public List<BookVO> getBooks() {
        return books;
    }

    /**
     * Listen to fired events from sender and this method is only called if the current instance of the bean
     * declaring the observer method already exists.
     *
     * @param book the event object fired by the {@link BookstoreService#addBook(BookVO)} method after persisting entity.
     */
    public void listenToBook(@Observes(notifyObserver = Reception.IF_EXISTS) final BookVO book) {
        loadViewBooks();
    }

    /**
     * Get a list of stored {@link BookVO}s.
     */
    private void loadViewBooks() {
        books = bookstoreService.getAllBooks();
    }
}
