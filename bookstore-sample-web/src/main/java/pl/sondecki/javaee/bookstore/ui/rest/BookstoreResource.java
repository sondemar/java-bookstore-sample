package pl.sondecki.javaee.bookstore.ui.rest;

import org.jboss.logging.Logger;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;
import pl.sondecki.javaee.bookstore.services.facade.BookstoreService;
import pl.sondecki.javaee.bookstore.ui.util.messages.MessagesManager;
import pl.sondecki.javaee.bookstore.ui.util.validation.book.BookValidator;
import pl.sondecki.javaee.bookstore.util.logging.Loggable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * REST service  with main path {@link BookstoreResource#PATH}
 * responsible for adding a new book, retrieving all stored
 * books and retrieving only one book based on ID identifier.
 *
 * @author Mariusz Sondecki
 */
@Path(BookstoreResource.PATH)
@RequestScoped
@Loggable
public class BookstoreResource {

    public static final String PATH = "/books";

    @Inject
    private BookstoreService bookstoreService;

    @Inject
    private BookValidator bookValidator;

    @Inject
    private MessagesManager messagesManager;

    @Inject
    private Logger logger;

    /**
     * Add a new entity {@link BookVO}. Fields of entity must fulfill validation requirements
     *
     * @param book entity of {@link BookVO}. Property {@link BookVO#getId()} is omitted during storing entity
     *             in the system.
     */
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void addBook(@Valid BookVO book) {

        bookValidator.validateBook(book);
        logger.debugf("Book bean: %s validated.", book);
        bookstoreService.addBook(book);

    }

    /**
     * Get a list of all stored {@link BookVO}s.
     *
     * @return A {@link List<BookVO>} containing all Books stored in the system.
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<BookVO> getAllBooks() {
        return bookstoreService.getAllBooks();
    }

    /**
     * Get a stored {@link BookVO}. If there is not any entity with specified ID, {@link ClientErrorException} is
     * thrown with {@link Response.Status#NOT_FOUND} status.
     *
     * @param id This is Long type and is required.
     * @return A Book containing all Books stored in the system.
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public BookVO getBook(@NotNull @PathParam("id") Long id) {
        BookVO book = bookstoreService.getBook(id);
        if (book == null) {
            throw new ClientErrorException(Response.Status.NOT_FOUND);
        }
        return book;
    }


}
