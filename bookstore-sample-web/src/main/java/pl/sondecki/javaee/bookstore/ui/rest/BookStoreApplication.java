package pl.sondecki.javaee.bookstore.ui.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAX-RS activator with main path {@link BookStoreApplication#PATH}
 *
 * @author Mariusz Sondecki
 */
@ApplicationPath(BookStoreApplication.PATH)
public class BookStoreApplication extends Application {
    public static final String PATH = "/rest";
}
