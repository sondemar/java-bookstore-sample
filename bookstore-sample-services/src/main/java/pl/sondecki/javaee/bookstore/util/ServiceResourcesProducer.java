package pl.sondecki.javaee.bookstore.util;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * This is util class responsible for creating commmon injection points,
 * for example {@link javax.persistence.EntityManager}ó
 *
 * @author Mariusz Sondecki
 */
public class ServiceResourcesProducer {

    @Produces
    @PersistenceContext
    private EntityManager em;
}
