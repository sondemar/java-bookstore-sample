package pl.sondecki.javaee.bookstore.ui.util.validation.book;

import pl.sondecki.javaee.bookstore.ui.util.validation.CommonValidationException;

/**
 * Application-specific exception. Thrown when validation
 * exceptions occur related to Book entity.
 *
 * @author Mariusz Sondecki
 */
public class BookValidationException extends CommonValidationException {
    public BookValidationException(String message) {
        super(message);
    }
}
