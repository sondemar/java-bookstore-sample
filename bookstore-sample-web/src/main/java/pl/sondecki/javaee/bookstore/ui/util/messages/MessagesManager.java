package pl.sondecki.javaee.bookstore.ui.util.messages;

import pl.sondecki.javaee.bookstore.ui.util.FacesContextDecorator;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Util for managing locale-specific messages. Messages are read from resource files which name convention is
 * the same like with {@link ResourceBundle}. Default name of messasage file is 'messages.properties'
 *
 * @author Mariusz Sondecki
 */
@RequestScoped
public class MessagesManager implements Serializable {

    public static final String MESSAGES = "messages";
    private ResourceBundle resourceBundle;

    @Inject
    private FacesContextDecorator facesContext;

    @PostConstruct
    private void init() {
        Locale locale = facesContext.getLocale();
        if (locale != null) {
            resourceBundle = ResourceBundle.getBundle(MESSAGES, locale);
        } else {
            resourceBundle = ResourceBundle.getBundle(MESSAGES);
        }

    }

    /**
     * Get message for key and object(s) to format (if exist).
     *
     * @param key
     * @param params object(s) to format
     * @return the simple string or the formatted string created by using returned message pattern
     * and params method argument
     */
    public String getMessage(String key, String... params) {
        String message = resourceBundle.getString(key);
        if (message != null && params != null && params.length > 0) {
            message = MessageFormat.format(message, params);
        }
        return message;
    }
}
