package pl.sondecki.javaee.bookstore.ui.util;

import javax.enterprise.inject.Specializes;
import javax.faces.application.FacesMessage;
import java.util.Locale;

/**
 * Mock class which specializes {@link FacesContextDecorator} during running test cases.
 * <p>
 *
 * @author Mariusz Sondecki
 */
@Specializes
public class MockFacesContextDecorator extends FacesContextDecorator {

    @Override
    public void addMessage(String clientId, FacesMessage m) {
    }

    @Override
    public Locale getLocale() {
        return null;
    }
}
