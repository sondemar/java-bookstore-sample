package pl.sondecki.javaee.bookstore.services.facade.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.commons.beanutils.BeanUtils;
import org.jboss.logging.Logger;

import pl.sondecki.javaee.bookstore.dao.common.book.BookDAO;
import pl.sondecki.javaee.bookstore.model.entity.db.book.Book;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;
import pl.sondecki.javaee.bookstore.services.facade.BookstoreService;
import pl.sondecki.javaee.bookstore.services.facade.BookstoreServiceException;
import pl.sondecki.javaee.bookstore.util.logging.Loggable;

/**
 * This is EJB which is implementation for {@link BookstoreService} interface.
 *
 * @author Mariusz Sondecki
 */
@Stateless
@Loggable
public class BookstoreServiceEJB implements BookstoreService {

    @Inject
    private Event<BookVO> bookEvent;

    @Inject
    private BookDAO<Book> bookDAO;

    @Inject
    private Logger logger;

    @Override
    public void addBook(BookVO book) {
        bookDAO.persist(fromVoToDatabase(book));
        logger.debugf("Fire observer event for Book entity: %s", book);
        bookEvent.fire(book);
    }

    @Override
    public BookVO getBook(long bookId) {
        return fromDatabaseToVO(bookDAO.get(bookId));
    }

    @Override
    public List<BookVO> getAllBooks() {
        List<BookVO> bookList = bookDAO.getAll().stream().map(this::fromDatabaseToVO).collect(Collectors.toList());
        return bookList;
    }

    @Override
    public List<BookVO> findByIsbn(String isbn) {
        List<BookVO> bookList = bookDAO.findByIsbn(isbn).stream().map(this::fromDatabaseToVO).collect(Collectors.toList());
        return bookList;
    }

    private BookVO fromDatabaseToVO(Book dbEntity) {
        BookVO vo = new BookVO();
        try {
            BeanUtils.copyProperties(vo, dbEntity);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error(null, e);
            throw new BookstoreServiceException(e);
        }
        return vo;
    }

    private Book fromVoToDatabase(BookVO voEntity) {
        Book dbEntity = new Book();
        try {
            BeanUtils.copyProperties(dbEntity, voEntity);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error(null, e);
            throw new BookstoreServiceException(e);
        }
        return dbEntity;
    }

}
