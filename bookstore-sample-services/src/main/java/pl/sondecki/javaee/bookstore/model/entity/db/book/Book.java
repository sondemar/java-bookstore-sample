package pl.sondecki.javaee.bookstore.model.entity.db.book;

import org.hibernate.validator.constraints.NotEmpty;
import pl.sondecki.javaee.bookstore.model.common.book.BookInterface;
import pl.sondecki.javaee.bookstore.util.constraints.AuthorForename;
import pl.sondecki.javaee.bookstore.util.constraints.AuthorSurname;
import pl.sondecki.javaee.bookstore.util.constraints.Isbn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Persistence entity for Book table in the database
 *
 * @author Mariusz Sondecki
 */
@Entity
public class Book implements BookInterface {

    @Id
    @GeneratedValue
    private Long id;
    @NotEmpty
    private String title;
    @Isbn
    @Column(unique = true)
    private String isbn;
    @AuthorForename
    private String forename;
    @AuthorSurname
    private String surname;

    public Book() {
    }


    public Book(String title, String isbn, String forename, String surname) {
        this.title = title;
        this.isbn = isbn;
        this.forename = forename;
        this.surname = surname;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getIsbn() {
        return isbn;
    }

    @Override
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String getForename() {
        return forename;
    }

    @Override
    public void setForename(String forename) {
        this.forename = forename;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        if (getId() != null ? !getId().equals(book.getId()) : book.getId() != null) return false;
        if (!getTitle().equals(book.getTitle())) return false;
        if (!getIsbn().equals(book.getIsbn())) return false;
        if (!getForename().equals(book.getForename())) return false;
        return getSurname().equals(book.getSurname());

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + getIsbn().hashCode();
        result = 31 * result + getForename().hashCode();
        result = 31 * result + getSurname().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", isbn='" + isbn + '\'' +
                ", forename='" + forename + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
