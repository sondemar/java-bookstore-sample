package pl.sondecki.javaee.bookstore.ui.util.messages;

import javax.enterprise.inject.Specializes;

/**
 * Mock class which specializes {@link MessagesManager} during running test cases.
 * <p>
 *
 * @author Mariusz Sondecki
 */
@Specializes
public class MockMessagesManager extends MessagesManager {

    @Override
    public String getMessage(String key, String... params) {
        return "";
    }
}
