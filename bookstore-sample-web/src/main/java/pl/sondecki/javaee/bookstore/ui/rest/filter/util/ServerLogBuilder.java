package pl.sondecki.javaee.bookstore.ui.rest.filter.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Log builder based on pattern: {@link ServerLogBuilder#Log_PATTERN}.
 *
 * @author Mariusz Sondecki
 */
public class ServerLogBuilder {

    /**
     * Log pattern: "Date","REST method STATUS","REST method","REST URL","Resource class","Resource method","Duration"
     */
    public static final String Log_PATTERN = "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%d\"";

    private static final String OPERATION_START_FORMAT = "yyyy-MM-dd HH:mm:ss,SSS";

    private Date startDate;
    private long duration;
    private String resourceClass;
    private String resourceMethod;
    private String url;
    private Integer statusCode;
    private String restMethod;
    private String status;

    /**
     * Append start date of request.
     * @param startDate
     * @return
     */
    public ServerLogBuilder appendStartDate(Date startDate) {
        this.startDate = Objects.requireNonNull(startDate);
        return this;
    }

    /**
     * Append REST method (for instance GET, POST, etc.)
     * @param restMethod
     * @return
     */
    public ServerLogBuilder appendRestMethod(String restMethod) {
        this.restMethod = restMethod;
        return this;
    }

    /**
     * Append Java REST resource class name which is invoked.
     * @param resourceClass
     * @return
     */
    public ServerLogBuilder appendResourceClass(String resourceClass) {
        this.resourceClass = resourceClass;
        return this;
    }

    /**
     * Append Java REST resource method which is evaluated as an endpoint.
     * @param resourceMethod
     * @return
     */
    public ServerLogBuilder appendResourceMethod(String resourceMethod) {
        this.resourceMethod = resourceMethod;
        return this;
    }

    /**
     * Append whole URL for requested REST service (protocol://host:port/path/resource)
     * @param url
     * @return
     */
    public ServerLogBuilder appendUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * Append REST method response status code (for instance 200, 400)
     * @param statusCode
     * @return
     */
    public ServerLogBuilder appendStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    /**
     * Append REST method response status (for instance OK, ERR)
     * @param status
     * @return
     */
    public ServerLogBuilder appendStatus(String status) {
        this.status = status;
        return this;
    }

    /**
     * Append duration in millis between request and response of invoked method.
     * @param duration
     * @return
     */
    public ServerLogBuilder appendDuration(long duration) {
        this.duration = duration;
        return this;
    }

    /**
     *
     * @return Log record wiht String type.
     */
    public String build() {
        return String.format(Log_PATTERN, new SimpleDateFormat(OPERATION_START_FORMAT)
                .format(startDate), status, restMethod, url, resourceClass, resourceMethod, duration);
    }

}
