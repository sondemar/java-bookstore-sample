package pl.sondecki.javaee.bookstore.dao.database.book;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import pl.sondecki.javaee.bookstore.dao.common.book.BookDAO;
import pl.sondecki.javaee.bookstore.model.entity.db.book.Book;

import javax.ejb.EJB;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Test case for DAO {@link BookDatabaseDAO}.
 * <p>
 * @author Mariusz Sondecki
 */
@RunWith(Arquillian.class)
public class BookDatabaseDAOTest {

    public static final String ISBN13 = "ISBN-10 0-596-52068-9";
    public static final String ISBN10 = "0-596-52068-9";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @EJB
    private BookDAO<Book> bookDAO;

    @Deployment
    public static Archive<?> deploy() {
        return ShrinkWrap.create(JavaArchive.class, "test.jar")
                .addPackages(true, "pl.sondecki.javaee.bookstore.dao")
                .addPackages(true, "pl.sondecki.javaee.bookstore.services")
                .addPackages(true, "pl.sondecki.javaee.bookstore.util")
                .addPackages(true, "pl.sondecki.javaee.bookstore.model")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    @InSequence(1)
    public void should_be_ready() {
        assertThat(bookDAO, is(notNullValue()));
    }

    @Test
    @InSequence(2)
    public void should_crud_books() {
        // Creates an objects
        Book book13 = new Book("title", ISBN13, "Adam", "Abramowski");
        Book book10 = new Book("title", ISBN10, "Adam", "Abba");

        // Stores created Book object
        bookDAO.persist(book13);
        bookDAO.persist(book10);

        // Gets created Book object with ISBN13
        List<Book> bookList = bookDAO.findByIsbn(ISBN13);
        assertThat(bookList.size(), equalTo(1));

        // Updates an object with ISBN13
        Book updatedBook = bookList.get(0);
        updatedBook.setTitle(updatedBook.getTitle() + "-updated");
        bookDAO.merge(updatedBook);
        assertThat(bookList.size(), equalTo(1));

        // Deletes an object with ISBN10
        bookList = bookDAO.findByIsbn(ISBN10);
        bookDAO.remove(bookList.get(0));

        // Gets all persisted Book objects
        bookList = bookDAO.getAll();
        assertThat(bookList.size(), equalTo(1));
    }



}
