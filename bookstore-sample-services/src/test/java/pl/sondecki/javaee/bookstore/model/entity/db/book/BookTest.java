package pl.sondecki.javaee.bookstore.model.entity.db.book;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.sondecki.javaee.bookstore.model.common.book.BookInterface;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;

import javax.inject.Inject;
import javax.validation.Validator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Test case for validation of {@link Book} database entity and {@link BookVO} value object.
 * <p>
 * @author Mariusz Sondecki
 */
@RunWith(Arquillian.class)
public class BookTest {

    @Inject
    private Validator validator;

    @Deployment
    public static Archive<?> deploy() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(Book.class).addClass(BookVO.class).addClass(BookInterface.class).addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    /**
     * Test {@link Book} and {@link BookVO} are correctly created.
     */
    @Test
    public void should_be_valid_books() {

        // Creates some objects
        Book book = new Book("title", "ISBN13 978-0-596-52068-7", "Adam", "Abramowski");
        BookVO bookVO = new BookVO(null, "title", "ISBN13 978-0-596-52068-7", "Adam", "Abramowski");

        // Checks the objects are valid
        assertThat(validator.validate(book).size(), equalTo(0));
        assertThat(validator.validate(bookVO).size(), equalTo(0));
    }

}
