package pl.sondecki.javaee.bookstore.ui.util;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Locale;

/**
 * This util class is decorator for {@link FacesContext}
 *
 * @author Mariusz Sondecki
 */
@SessionScoped
public class FacesContextDecorator implements Serializable {

    /**
     * Add message to the {@link FacesContext}
     *
     * @param clientId UI id
     * @param m
     */
    public void addMessage(String clientId, FacesMessage m) {
        getContext().addMessage(clientId, m);
    }

    /**
     * Get the {@link Locale} for the root component that is associated with the this request.
     *
     * @return
     */
    public Locale getLocale() {
        return getContext().getViewRoot().getLocale();
    }

    /**
     * @return the current faces context.
     */
    private FacesContext getContext() {
        return FacesContext.getCurrentInstance();
    }

}
