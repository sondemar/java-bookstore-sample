package pl.sondecki.javaee.bookstore.services.facade;


import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;

import java.util.List;

/**
 * This is facade service which is responsible for managing business processing rules and logic.
 *
 * @author Mariusz Sondecki
 */
public interface BookstoreService {

    /**
     * Add a new book instance.
     *
     * @param book
     */
    void addBook(BookVO book);

    /**
     * Get a book from system with unique, not null bookId.
     *
     * @param bookId
     * @return
     */
    BookVO getBook(long bookId);

    /**
     * Get a list of all existing book instances.
     *
     * @return
     */
    List<BookVO> getAllBooks();

    /**
     * Get a list of existing books with unique ISBN. Correctly only one instace in the list should be returned.
     *
     * @param isbn
     * @return
     */
    List<BookVO> findByIsbn(String isbn);

}
