package pl.sondecki.javaee.bookstore.util.constraints;

import pl.sondecki.javaee.bookstore.util.constraints.validators.ValidAuthor;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.*;

/**
 * Custom constraint stereotype which validates author surname and is based on {@link ValidAuthor}
 *
 * @author Mariusz Sondecki
 */
@Documented
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Constraint(validatedBy = {})
@ValidAuthor
public @interface AuthorSurname {

    String message() default "{pl.sondecki.javaee.bookstore.util.constraints.AuthorSurname.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
