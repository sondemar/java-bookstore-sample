package pl.sondecki.javaee.bookstore.dao.database;

import org.jboss.logging.Logger;
import pl.sondecki.javaee.bookstore.dao.common.CommonDAO;

import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * This is abstract database DAO class for a specific Entity.
 *
 * @param <DBEntity> Class of the Entity the DAO will mainly deal with.
 * @author Mariusz Sondecki
 */
public abstract class AbstractDatabaseDAO<DBEntity> implements CommonDAO<DBEntity> {

    private final Class<DBEntity> entityClass;

    @Inject
    private Logger logger;

    public AbstractDatabaseDAO(Class<DBEntity> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public DBEntity get(Long id) {
        return getEntityManager().find(entityClass, id);
    }

    @Override
    public void persist(DBEntity dbEntity) {
        getEntityManager().persist(dbEntity);
    }

    @Override
    public void remove(DBEntity dbEntity) {
        getEntityManager().remove(getEntityManager().merge(dbEntity));
    }

    @Override
    public void merge(DBEntity dbEntity) {
        getEntityManager().merge(dbEntity);
    }

    /**
     * Get logger
     * @return
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * Get persistence entity manager
     *
     * @return
     */
    protected abstract EntityManager getEntityManager();

}
