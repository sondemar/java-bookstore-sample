package pl.sondecki.javaee.bookstore.services.facade;

import javax.ejb.EJBException;


public class BookstoreServiceException extends EJBException {
    public BookstoreServiceException(Exception ex) {
        super(ex);
    }
}
