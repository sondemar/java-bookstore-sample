package pl.sondecki.javaee.bookstore.util.logging;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.*;

/**
 * Created by Mariusz Sondecki on 2015-07-30.
 */
@InterceptorBinding
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface Loggable {
}
