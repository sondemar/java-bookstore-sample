package pl.sondecki.javaee.bookstore.util;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.jboss.logging.Logger;

/**
 * This is util class responsible for creating commmon injection points,
 * for example {@link org.jboss.logging.Logger}
 *
 * @author Mariusz Sondecki
 */
public class ResourcesProducer {
	
    @Produces
    public Logger produceLog(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }
}
