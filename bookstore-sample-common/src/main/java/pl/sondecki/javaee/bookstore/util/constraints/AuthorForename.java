package pl.sondecki.javaee.bookstore.util.constraints;

import pl.sondecki.javaee.bookstore.util.constraints.validators.ValidAuthor;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Custom constraint stereotype which validates author forename and is based on {@link ValidAuthor}
 *
 * @author Mariusz Sondecki
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
@Constraint(validatedBy = {})
@ValidAuthor
public @interface AuthorForename {

    String message() default "{pl.sondecki.javaee.bookstore.util.constraints.AuthorForename.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
