package pl.sondecki.javaee.bookstore.model.vo.book;

import org.hibernate.validator.constraints.NotEmpty;
import pl.sondecki.javaee.bookstore.model.common.book.BookInterface;
import pl.sondecki.javaee.bookstore.util.constraints.AuthorForename;
import pl.sondecki.javaee.bookstore.util.constraints.AuthorSurname;
import pl.sondecki.javaee.bookstore.util.constraints.Isbn;

import javax.enterprise.inject.Vetoed;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Value object which is lightweight structure that facilitate display of business information.
 * This class is not injected by CDI container and should be produced.
 *
 * @author Mariusz Sondecki
 */
@XmlRootElement
@Vetoed
public class BookVO implements BookInterface, Serializable {

    private Long id;

    @NotEmpty
    private String title;

    @Isbn
    private String isbn;

    @AuthorForename
    private String forename;

    @AuthorSurname
    private String surname;

    public BookVO() {
    }

    public BookVO(Long id, String title, String isbn, String forename, String surname) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.forename = forename;
        this.surname = surname;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getIsbn() {
        return isbn;
    }

    @Override
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String getForename() {
        return forename;
    }

    @Override
    public void setForename(String forename) {
        this.forename = forename;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookVO)) return false;

        BookVO book = (BookVO) o;

        if (getId() != null ? !getId().equals(book.getId()) : book.getId() != null) return false;
        if (!getTitle().equals(book.getTitle())) return false;
        if (!getIsbn().equals(book.getIsbn())) return false;
        if (!getForename().equals(book.getForename())) return false;
        return getSurname().equals(book.getSurname());

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + getTitle().hashCode();
        result = 31 * result + getIsbn().hashCode();
        result = 31 * result + getForename().hashCode();
        result = 31 * result + getSurname().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "BookVO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", isbn='" + isbn + '\'' +
                ", forename='" + forename + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
