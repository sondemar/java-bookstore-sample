package pl.sondecki.javaee.bookstore.ui.controller;

import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import java.io.Serializable;

/**
 * Mock class which simulates observer event consumer during running test cases.
 * <p>
 *
 * @author Mariusz Sondecki
 */
@SessionScoped
public class MockBookViewReceiver implements Serializable {

    private BookVO receivedBookVO;

    public void receive(@Observes final BookVO book) {
        this.receivedBookVO = new BookVO(book.getId(), book.getTitle(), book.getIsbn(), book.getForename(), book.getSurname());
    }

    public BookVO getReceivedBookVO() {
        return receivedBookVO;
    }
}
