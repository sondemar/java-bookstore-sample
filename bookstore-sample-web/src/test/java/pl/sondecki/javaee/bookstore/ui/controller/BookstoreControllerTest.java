package pl.sondecki.javaee.bookstore.ui.controller;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;

import javax.inject.Inject;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Test case for the View Bean {@link BookstoreController}.
 * <p>
 *
 * @author Mariusz Sondecki
 */
@RunWith(Arquillian.class)
public class BookstoreControllerTest {

    @Inject
    private BookVO bookVO;

    @Inject
    private BookstoreController bookstoreController;

    @Inject
    private MockBookViewReceiver mockBookViewReceiver;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackages(true, BookstoreController.class.getPackage())
                .addPackages(true, "pl.sondecki.javaee.bookstore.ui.util")
                .addPackages(true, "pl.sondecki.javaee.bookstore.model")
                .addPackage("pl.sondecki.javaee.bookstore.services.facade")
                .addPackages(true, "pl.sondecki.javaee.bookstore.util")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource(EmptyAsset.INSTANCE, "messages.properties")
                .addAsWebInfResource("test-beans.xml", "beans.xml");
    }

    @Test
    @InSequence(1)
    public void should_be_ready() {
        assertThat(bookstoreController, is(notNullValue()));
    }


    @Test
    @InSequence(2)
    public void should_add_book() throws Exception {

        // Injects values inside managed bean BookVO (produced via BookstoreController#getNewBook() method)
        bookVO.setForename("Aforename");
        bookVO.setIsbn("ISBN-10 0-596-52068-9");
        bookVO.setSurname("Asurname");
        bookVO.setTitle("title");

        // Adds the book only via EJB service (bookstoreController.setByRest(false)), because included implementation for REST is temporary
        bookstoreController.addBook();

        final BookVO receivedBookVO = mockBookViewReceiver.getReceivedBookVO();


        assertThat(receivedBookVO, equalTo(bookVO));
    }
}
