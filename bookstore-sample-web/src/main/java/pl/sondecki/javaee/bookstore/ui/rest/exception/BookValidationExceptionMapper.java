package pl.sondecki.javaee.bookstore.ui.rest.exception;

import pl.sondecki.javaee.bookstore.ui.util.validation.book.BookValidationException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Provider mapping {@link BookValidationException} exception to {@link Response}.
 *
 * @author Mariusz Sondecki
 */
@Provider
public class BookValidationExceptionMapper implements ExceptionMapper<BookValidationException> {

    @Override
    public Response toResponse(BookValidationException exception) {
        return Response
                .status(Response.Status.CONFLICT)
                .entity(exception.getMessage())
                .build();
    }
}
