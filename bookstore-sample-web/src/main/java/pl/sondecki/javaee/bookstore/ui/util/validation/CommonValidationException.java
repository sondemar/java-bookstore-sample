package pl.sondecki.javaee.bookstore.ui.util.validation;

import javax.validation.ValidationException;

/**
 * Application-specific exception. Thrown when validation
 * exceptions occur.
 *
 * @author Mariusz Sondecki
 */
public abstract class CommonValidationException extends ValidationException {

    public CommonValidationException(String message) {
        super(message);
    }

    public enum TYPE {VALIDATION, OTHER}

    ;
}
