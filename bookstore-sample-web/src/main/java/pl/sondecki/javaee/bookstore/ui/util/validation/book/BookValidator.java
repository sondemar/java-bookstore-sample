package pl.sondecki.javaee.bookstore.ui.util.validation.book;

import org.jboss.logging.Logger;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;
import pl.sondecki.javaee.bookstore.services.facade.BookstoreService;
import pl.sondecki.javaee.bookstore.ui.util.messages.MessagesManager;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.Validator;
import java.util.List;

/**
 * Custom validator for {@link BookVO} instance.
 *
 * @author Mariusz Sondecki
 */
@RequestScoped
public class BookValidator {

    @Inject
    private Validator validator;

    @Inject
    private Logger logger;

    @Inject
    private BookstoreService bookstoreService;

    @Inject
    private MessagesManager messagesManager;

    /**
     * Validate {@link BookVO} based on checking that there is not another
     * instance with the same unique {@link BookVO#getIsbn()} property like book argument. If exists then
     * {@link BookValidationException} is thrown.
     *
     * @param book
     * @throws BookValidationException
     */
    public void validateBook(BookVO book) throws BookValidationException {
        if (isbnAlreadyExists(book.getIsbn())) {
            throw new BookValidationException(messagesManager.getMessage("addbook.error.uniqueisbn.summary"));
        }
    }

    /**
     * Check exists any Book entity in the system with the same ISBN like {@code isbn} argument
     *
     * @param isbn
     * @return
     */
    public boolean isbnAlreadyExists(String isbn) {
        List<BookVO> books = bookstoreService.findByIsbn(isbn);
        return books != null && books.size() > 0;
    }
}
