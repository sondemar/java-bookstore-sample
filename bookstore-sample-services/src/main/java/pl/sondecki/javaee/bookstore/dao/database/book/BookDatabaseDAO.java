package pl.sondecki.javaee.bookstore.dao.database.book;

import pl.sondecki.javaee.bookstore.dao.common.book.BookDAO;
import pl.sondecki.javaee.bookstore.dao.database.AbstractDatabaseDAO;
import pl.sondecki.javaee.bookstore.model.entity.db.book.Book;
import pl.sondecki.javaee.bookstore.model.entity.db.book.Book_;
import pl.sondecki.javaee.bookstore.util.logging.Loggable;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * This is EJB which is implementation for {@link BookDAO<Book>} interface, responsible for managing database.
 *
 * @author Mariusz Sondecki
 */
@Stateless
@Loggable
public class BookDatabaseDAO extends AbstractDatabaseDAO<Book> implements BookDAO<Book> {

    @Inject
    private EntityManager em;

    public BookDatabaseDAO() {
        super(Book.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public List<Book> findByIsbn(String isbn) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        final Root<Book> root = cq.from(Book.class);
        cq.select(root);
        cq.where(cb.equal(root.get(Book_.isbn), isbn)).distinct(true);
        final List<Book> resultList = em.createQuery(cq).getResultList();

        getLogger().debugf("Records for isbn: %s are: %s", isbn, resultList);

        return resultList;
    }

    @Override
    public List<Book> getAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> cq = cb.createQuery(Book.class);
        final Root<Book> root = cq.from(Book.class);
        cq.select(root);
        cq.orderBy(cb.asc(root.get(Book_.forename)), cb.asc(root.get(Book_.surname)));

        return em.createQuery(cq).getResultList();
    }
}
