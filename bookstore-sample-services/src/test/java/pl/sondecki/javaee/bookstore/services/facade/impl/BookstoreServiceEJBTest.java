package pl.sondecki.javaee.bookstore.services.facade.impl;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.sondecki.javaee.bookstore.dao.database.book.MockBookDatabaseDAO;
import pl.sondecki.javaee.bookstore.model.vo.book.BookVO;
import pl.sondecki.javaee.bookstore.services.facade.BookstoreService;

import javax.ejb.EJB;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Test case for the EJB facade {@link BookstoreServiceEJB}.
 * <p>
 * @author Mariusz Sondecki
 */
@RunWith(Arquillian.class)
public class BookstoreServiceEJBTest {

    @EJB
    private BookstoreService bookstoreService;

    @Deployment
    public static Archive<?> deploy() {
        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true, "pl.sondecki.javaee.bookstore.dao")
                .addPackages(true, "pl.sondecki.javaee.bookstore.services")
                .addPackages(true, "pl.sondecki.javaee.bookstore.util")
                .addPackages(true, "pl.sondecki.javaee.bookstore.model")
                .addPackages(true, "org.apache.commons.beanutils")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsManifestResource("test-beans.xml", "beans.xml");
    }

    @Test
    @InSequence(1)
    public void should_be_ready() {
        assertThat(bookstoreService, is(notNullValue()));
    }


    @Test
    @InSequence(2)
    public void should_ceate_and_find_books() {
        // Adds sone new valid  books
        bookstoreService.addBook(new BookVO(null, "title", MockBookDatabaseDAO.ISBN_BEAN1, "Adam", "Abramowski"));
        bookstoreService.addBook(new BookVO(null, "title", MockBookDatabaseDAO.ISBN_BEAN2, "Adam2", "Abramowski2"));

        // Gets created Book object with MockBookDatabaseDAO.ISBN_BEAN1
        List<BookVO> bookList = bookstoreService.findByIsbn(MockBookDatabaseDAO.ISBN_BEAN1);
        assertThat(bookList.size(), equalTo(1));

        // Gets all persisted Book objects
        bookList = bookstoreService.getAllBooks();
        assertThat(bookList.size(), equalTo(2));
    }
}
